<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable=['invoice_id','client_id','company_info','cart_items','subtotal','discount','tax','grand_total','notes','terms','bg_image','invoice_no','issue_date','due_date','reference','client_info','paid_amount','due_amount','status'];

    protected $casts=['company_info'=>'array', 'client_info'=>'array', 'cart_items'=>'array'];

    public function client(){
        return $this->belongsTo('App\User','client_id');
    }
}
