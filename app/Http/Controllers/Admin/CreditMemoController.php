<?php

namespace App\Http\Controllers\Admin;

use App\CreditMemo;
use App\Invoice;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CreditMemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $credit_memos=CreditMemo::orderBy('id','desc')->get();
        return view('admin.credit_memo.index',compact('credit_memos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('https://invoices-manage.herokuapp.com/credit-memo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $credit_memo=CreditMemo::where('credit_memo_id',$id)->firstOrFail();
        return view('admin.credit_memo.show',compact('credit_memo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CreditMemo::destroy($id);
        flash('Credit Memo deleted successfully');
        return redirect()->action('Admin\CreditMemoController@index');
    }

    public function download_pdf($id){
        $credit_memo=CreditMemo::where('credit_memo_id',$id)->firstOrFail();
        $pdf = PDF::loadView('admin.credit_memo.pdf', compact('credit_memo'));
        return $pdf->download('credit_memo.pdf');
    }
}
