<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name'=> $this->business_contact_first_name,
            'last_name'=> $this->business_contact_last_name,
            'nick_name'=> strtoupper(substr($this->business_contact_first_name,0,1) .substr($this->business_contact_last_name,0,1)),
            'company'=> $this->company_name,
            'street'=> $this->street,
            'city'=> $this->city,
            'zipcode'=> $this->zipcode,
            'country'=> $this->country,
        ];
    }
}
