<!DOCTYPE html>

<html lang="en" class="smart-style-6">
@inject('request', 'Illuminate\Http\Request')
@include('admin.partials.head')

<body class="smart-style-6">

<!-- BEGIN .sa-wrapper -->
<div class="sa-wrapper">
    <!-- BEGIN .sa-shortcuts -->

    <div class="sa-shortcuts-section">
        <ul>
            <li><a class="bg-pink-dark selected" href=""><span class="fa fa-user fa-4x"></span><span class="box-caption">My Profile</span><em class="counter"></em></a></li>
        </ul>
    </div>
    <!-- END .sa-shortcuts -->

    @include('admin.partials.header')
    <!-- END .sa-page-header -->
    <div class="sa-page-body">
        <!-- BEGIN .sa-aside-left -->
        @include('admin.partials.left_sidebar')

        <!-- BEGIN .sa-content-wrapper -->
        <div class="sa-content-wrapper">
            @yield('contents')
            <!-- BEGIN .sa-page-footer -->
            @include('admin.partials.footer')
            <!-- END .sa-page-footer -->
        </div>
        <!-- END .sa-content-wrapper -->
    </div>
</div>
<!-- END .sa-wrapper -->

<script src="/ic_admin/js/vendors.bundle.js"></script>
<script src="/ic_admin/js/app.bundle.js"></script>

<script>
    $(function () {
        $('#menu1').metisMenu();

        $(".sa-logout-header-toggle").click(function(t) {
            var a = $(this);
            userLogout(a);
            t.preventDefault();
            a = null;
        })
    });
</script>

<script type="text/javascript">

    // DO NOT REMOVE : GLOBAL FUNCTIONS!


    /* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
			*/

    /* BASIC ;*/
    var responsiveHelper_dt_basic = responsiveHelper_dt_basic || undefined;

    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar d-flex'<f><'ml-auto hidden-xs show-control'l>r>"+
            "t"+
            "<'dt-toolbar-footer d-flex'<'hidden-xs'i><'ml-auto'p>>",
        "autoWidth" : true,
        "oLanguage": {
            "sSearch": '<span class="input-group-addon"><i class="fa fa-search"></i></span>'
        },
        classes: {
            sWrapper:      "dataTables_wrapper dt-bootstrap4"
        },
        responsive: true
    });

    /* END BASIC */

</script>

@yield('script')
</body>
</html>
