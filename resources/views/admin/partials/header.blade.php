<header class="sa-page-header">
    <div class="sa-header-container h-100">
        <div class="d-table d-table-fixed h-100 w-100">
            <div class="sa-logo-space d-table-cell h-100">
                <div class="flex-row d-flex align-items-center h-100">
                    <a class="sa-logo-link" href="" title="Invoice Management v1.0"><img alt="Invoice Management v1.0" src="/ic_admin/img/common/logo.png" class="sa-logo"></a>
                    {{--<div class="dropdown ml-auto">--}}
                        {{--<button class="btn btn-default sa-btn-icon sa-activity-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span><span class="badge bg-red">21</span></button>--}}
                        {{--<div class="dropdown-menu ml-auto ajax-dropdown" aria-labelledby="dropdownMenuButton">--}}
                            {{--<form class="btn-group btn-group-justified" role="group" aria-label="Basic example">--}}
                                {{--<button type="button" class="btn btn-default" onclick="loadNotifications(this)" data-url="ajax/mail.html">Msgs (21)</button>--}}
                                {{--<button type="button" class="btn btn-default" onclick="loadNotifications(this)" data-url="ajax/notifications.html">Notify (3)</button>--}}
                                {{--<button type="button" class="btn btn-default" onclick="loadNotifications(this)" data-url="ajax/tasks.html">Tasks (4)</button>--}}
                            {{--</form>--}}
                            {{--<div class="sa-ajax-notification-container">--}}
                                {{--<div class="alert sa-ajax-notification-alert">--}}
                                    {{--<h4>Click a button to show messages here</h4>--}}
                                    {{--This blank page message helps protect your privacy, or you can show the first message here automatically.--}}
                                {{--</div>--}}
                                {{--<span class="fa fa-lock fa-4x fa-border"></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="d-table-cell h-100 w-100 align-middle">
                <div class="sa-header-menu">
                    <div class="d-flex align-items-center w-100">
                        <div class="ml-auto sa-header-right-area">
                            <div class="form-inline">
                                <button class="btn btn-default sa-logout-header-toggle sa-btn-icon" type="button"><span class="fa fa-sign-out"></span></button>
                                <button class="btn btn-default sa-btn-icon sa-sidebar-hidden-toggle" onclick="SAtoggleClass(this, 'body', 'sa-hidden-menu')" type="button"><span class="fa fa-reorder"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</header>
