<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link href="/images/favicon.png" rel="icon">

    <!-- fonts -->

    <!-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/style.css" rel="stylesheet">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script src="/js/jquery-1.12.4.min.js"></script>
    <!-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> -->
    <style>
        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
</head>
<body id="body">
<main>
    <div class="full-page ic-ready-page">
        <div class="container">
            <form>
                <div class="ic-invoice-wrapper">
                    <!-- top heading -->
                    <header id="heading" class="ic-top-heading p-0 no-print">
                        <div class="title">
                            <h1>Invoice {{ $invoice->invoice_no }}</h1>
                        </div>
                        <div class="button-grp">
                            <!-- more action with dropdown -->
                            <div class="ic-add-discount-full ic-tax ic-more-action-full">
                                <div class="ic-add-discount-btn">
                                    <button id="t-more" class="ic-btn ic-btn-primary">More Actions</button>
                                </div>
                                <div class="ic-add-discount-dropdown">
                                    <div class="ic-body-discount ic-discount">
                                        <a  class="ic-pdf" href="{{ action('Admin\InvoiceController@download_pdf',$invoice->invoice_id) }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            Download pdf
                                        </a>
                                        <button class="ic-pdf" id="print-invoice">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                            Print
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- more action with dropdown end -->
                        </div>
                    </header>
                    <!-- top heading end -->
                    <!-- invoice -->
                    <section class="ic-invoice p-0">
                        <div class="wrapper">
                            <div class="heading">
                                <div class="ic-form-inner">
                                    <div class="ic-left">
                                        <div class="form-group">
                                            <h2 class="mb-2">{{ $invoice->company_info['company_name'] }}</h2>
                                            {{ $invoice->company_info['phone_no'] }}
                                        </div>
                                    </div>
                                    <div class="ic-right">
                                        <div class="inner-text-ic-right ic-white ic-font-style">
                                            {{ $invoice->company_info['street'] }}<span> <br></span>{{ $invoice->company_info['city'] }} - {{ $invoice->company_info['zipcode'] }}<br>
                                            <span class="js-sender-address-country"><span>{{ $invoice->company_info['country'] }}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- invoice banner -->
                            <div class="ic-invoice-banner">
                                <div class="inner">
                                    <img src="{{ (!is_null($invoice->bg_image))? '/storage/'.$invoice->bg_image : '/images/support.png' }}" id="banner-img"/>
                                </div>
                            </div>
                            <!-- invoice banner end-->
                            <!-- invoice body -->
                            <div class="ic-invoice-body">
                                <div class="inner">
                                    <!-- sidebar -->
                                    <aside class="ic-invoice-sidebar">
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Amount Due (USD)</div>
                                            <div class="ic-invoice-totoal">${{ $invoice->due_amount }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar ic-add-client-main">
                                            <div class="ic-text ic-invoice-label">Billed To</div>
                                            <div class="ic-billed-to-text">
                                                <span class="">{{ $invoice->client_info['first_name'] ?? '' . $invoice->client_info['last_name']  ?? '' }}</span><br>
                                                <span class="">{{ $invoice->client_info['company'] ?? '' }}</span><br>
                                                <span class="">{{ $invoice->client_info['street'] ?? '' }}</span><br>
                                                <span class="">{{ $invoice->client_info['city'] ?? '' }}</span><span> - </span><span class="">{{ $invoice->client_info['zipcode'] ?? '' }}</span><br>
                                                <span>{{ $invoice->client_info['country'] ?? '' }}</span>
                                            </div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Invoice Number</div>
                                            <div class="ic-invoice-number">{{ $invoice->invoice_no }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Date of Issue</div>
                                            <div class="ic-dat-of-issue">{{ $invoice->issue_date }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Due Date</div>
                                            <div class="ic-due-date">{{ $invoice->due_date }}</div>
                                        </div>
                                        <!-- single block -->
                                        <div class="ic-single-block-asidebar">
                                            <div class="ic-text ic-invoice-label">Reference</div>
                                            <div class="ic-referrence">{{ $invoice->reference }} </div>
                                        </div>
                                    </aside>
                                    <article class="ic-invoice-content">
                                        <div class="app">
                                            <table class="table">
                                                <tr>
                                                    <th style="width:30%" class="ic-text ic-invoice-label">Description</th>
                                                    <th  style="width:25%" class="ic-text ic-invoice-label">Rate</th>
                                                    <th  style="width:20%" class="ic-text ic-invoice-label">Qty</th>
                                                    <th style="width:20%" class="ic-text ic-invoice-label">Line Total</th>
                                                    <th  style="width:5%" class="ic-text ic-invoice-label last"></th>
                                                </tr>
                                                @foreach($invoice->cart_items as $cart_item)
                                                    <tr>
                                                        <td  style="width:30%" >
                                                            <div class="ic-item-name">{{ $cart_item['name'] }}</div>
                                                            <div class="ic-input-description">{{ $cart_item['description'] }}</div>
                                                        </td>
                                                        <td style="width:25%">
                                                            <div class="ic-rate">${{ $cart_item['rate'] }}</div>
                                                        </td>
                                                        <td style="width:20%">
                                                            <div class="ic-qty">{{ $cart_item['quantity'] }}</div>
                                                        </td>
                                                        <td style="width:20%">
                                                            <div class="ic-total">${{ $cart_item['rate'] * $cart_item['quantity'] }}</div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="tax-and-discount">
                                            <div class="col-one">
                                                <div class="col-one-inner">
                                                    <div>subtotal</div>
                                                    <div class="ic-add-discount-full">
                                                        Discount
                                                    </div>
                                                    <div class="ic-add-discount-full ic-tax">
                                                        Tax
                                                    </div>
                                                </div>
                                                <div class="ic-totoal">Total</div>
                                            </div>
                                            <div class="col-two">
                                                <div class="col-two-inner">
                                                    <div class="ic-subtotal">${{ $invoice->subtotal }}</div>
                                                    <div class="ic-discount-text">${{ $invoice->discount }}</div>
                                                    <div class="ic-tax-text" ><span>${{ $invoice->tax }}</span></div>
                                                </div>
                                                <div class="ic-for-totoal">${{ $invoice->grand_total }}</div>
                                            </div>
                                        </div>
                                        <!-- single block -->
                                        @if(!is_null($invoice->notes))
                                            <div class="ic-single-block-contentbar">
                                                <div class="ic-text ic-invoice-label">Notes</div>
                                                <div class="ic-notes">{{ $invoice->notes }}</div>
                                            </div>
                                        @endif
                                        <!-- single block -->
                                        @if(!is_null($invoice->terms))
                                            <div class="ic-single-block-contentbar">
                                                <div class="ic-text ic-invoice-label">Terms</div>
                                                <div class="ic-terms">{{ $invoice->terms }}</div>
                                            </div>
                                        @endif
                                    </article>
                                </div>
                            </div>
                            <!-- invoice body end-->
                        </div>
                    </section>
                    <!-- invoice end -->
                </div>
            </form>
        </div>
    </div>
</main>


<!-- Required JavaScript Libr
aries -->

<!-- script -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/plugins.min.js"></script>
<script src="/js/custom.min.js"></script>
<script>
    $('#print-invoice').click(function() {
        window.print();
    });
</script>
</body>
</html>
