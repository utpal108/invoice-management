@extends('admin.layout.app')

@section('page_title','Admin | Edit site settings')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\SiteSettingController@index') }}">Site Settings</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Site Settings</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                @include('flash::message')
                <form id="sliderGroup" action="{{ action('Admin\SiteSettingController@update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Site Settings </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Site Settings
                                            </legend>
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input type="text" class="form-control" name="company_name" value="{{ setting('company_name') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <input type="text" class="form-control" name="phone_no" value="{{ setting('phone_no') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ setting('email') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Business Name</label>
                                                <input type="text" class="form-control" name="business_name" value="{{ setting('business_name') }}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Website</label>
                                                <input type="url" class="form-control" name="website" value="{{ setting('website') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Street</label>
                                                <input type="text" class="form-control" name="street" value="{{ setting('street') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="city" value="{{ setting('city') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Zipcode</label>
                                                <input type="text" class="form-control" name="zipcode" value="{{ setting('zipcode') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" class="form-control" name="Country" value="{{ setting('Country') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Login Page Image(570X500)</label>
                                                <div class="box-body text-center">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                            <img src="@if(!is_null(setting('login_image_01'))){{ '/storage/' .setting('login_image_01') }} @else{{ 'http://placehold.it/150X150' }} @endif" width="100%" alt="login image">
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                                <input type="file" name="login_image_01" @if(is_null(setting('login_image_01'))){{ 'required' }} @endif>
                                                            </span>
                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Logo(130X70):</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="@if(!is_null(setting('logo'))){{ '/storage/' .setting('logo') }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="slider image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="logo">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    {{--<h4 class="modal-title">Set Location</h4>--}}
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 100%; height: 450px;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        var currentLat =23.758206;
        var currentLong =90.385208;
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition);
        }

        function showPosition(position) {
            currentLat=position.coords.latitude;
            currentLong=position.coords.longitude;
        }

        function setPosition(position) {
            document.getElementById('current_lat').value=position.lat();
            document.getElementById('current_long').value=position.lng();
        }

        function initMap() {
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                zoom: 12,
                center: new google.maps.LatLng(currentLat, currentLong)
            });

            var myLatlng = {lat: currentLat, lng: currentLong};

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Click to zoom',
                draggable: true,
            });

            // Zoom to 9 when clicking on marker
            google.maps.event.addListener(marker,'click',function() {
                map.setZoom(9);
                map.setCenter(marker.getPosition());
            });

            google.maps.event.addListener(map, 'click', function(event) {
                marker.setPosition(event.latLng);
                setPosition(event.latLng);
            });

            google.maps.event.addListener(map, 'drag', function() {
                marker.setPosition(map.getCenter());
            } );

            google.maps.event.addListener(map, 'dragend', function() {
                setPosition(map.getCenter());
            } );

            marker.addListener('dragend', function (event) {
                setPosition(event.latLng);
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNrASOuKNnXr4a5rEqDC_8fo_isduYSeU&callback=initMap&sensor=true">
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#myMapModal').on('shown.bs.modal', function(e) {
                var element = $(e.relatedTarget);
                var data = element.data("lat").split(',');
                initialize(new google.maps.LatLng(data[0], data[1]));
                google.maps.event.trigger(map, 'resize');
            });

            var contact_info_length=$('.single-contact-info').length;
            //    Add Form Elements
            $('#add_contact_info').click(function () {
                var data='<fieldset class="single-contact-info">\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Contact Title</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][title]" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Address</label>\n' +
                    '                        <textarea class="form-control" name="contents[contact_info]['+contact_info_length+'][address]" rows="8" required></textarea>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Phone Numbers</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][phone_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Mobile Numbers</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][mobile_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <button class="btn btn-danger pull-right" onclick="if(confirm(\'Are You Sure ?\')){$(this).parent().parent().remove()}">Remove</button>\n' +
                    '                    </div>\n' +
                    '                </fieldset>';
                $('#contact-info-group').append(data);

                contact_info_length++;
            });

            var socialElements= $('.eachSocialIcon').length;
            $('#addSocialIcon').click(function () {
                var data ='<div class="col-md-12 eachSocialIcon mt-3">\n' +
                    '                                                    <div class="input-group input-group-md">\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][name]" placeholder="Name" required>\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][icon]" placeholder="Icon" required>\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][url]" placeholder="URL" required>\n' +
                    '                                                        <div class="input-group-append">\n' +
                    '                                                            <button class="btn btn-default" type="button" onclick="if(confirm(\'Are You Sure ?\')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>';
                $('#socialIconGroup').append(data);

                socialElements++;
            });


            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

        })
    </script>
    {{-- For Flash message--}}
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
