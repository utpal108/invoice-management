<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="#" type="image/png">
    <title>Login Form</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/ic_admin/login_form/css/bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/login_form/css/font-awesome.min.css">
    <link rel="stylesheet" href="/ic_admin/login_form/vendors/linericon/style.css">

    <!-- Extra Plugin CSS -->

    <!-- main css -->
    <link rel="stylesheet" href="/ic_admin/login_form/css/style.css">
    <link rel="stylesheet" href="/ic_admin/login_form/css/responsive.css">
    <style>
        .ic_main_form_area:before {
            {{--background: url({{ '/storage/' .setting('login_image_02') }}) no-repeat scroll center center;--}}
        }
    </style>
</head>
<body class="body_color">

<!--================Login Form Area =================-->
<section class="ic_main_form_area" >
    <div class="container">
        <div class="ic_main_form_inner">
            <div class="row">
                <div class="col-lg-6 col-md-5">
                    <div class="form_img">
                        <img src="{{ '/storage/' .setting('login_image_01') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 d-flex">
                    <div class="form_box">
                        <img class="img-fluid" src="{{ '/storage/' .setting('logo') }}" alt="">
                        <p>Experience the simplicity Invoice Management System</p>
                        @include('flash::message')
                        <form class="row login_form" action="{{ action('Admin\UserController@authenticate') }}" method="post" id="contactForm" novalidate="novalidate">
                            @csrf
                            <div class="form-group col-lg-12">
                                <input type="email" class="form-control" id="name" name="email" placeholder="Enter Email">
                                <i class="fa fa-user-o"></i>
                            </div>
                            <div class="form-group col-lg-12">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            </div>
                            <div class="form-group remember col-lg-12">
                                <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" name="remember_token" value="true">
                                <label for="styled-checkbox-2">Remember Me</label>
                                <a href="/login">Forgot Password?</a>
                            </div>
                            <div class="form-group col-lg-12">
                                <button type="submit" value="submit" class="btn submit_btn form-control">Login in my account</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Login Form Area =================-->



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/ic_admin/login_form/js/jquery-3.3.1.min.js"></script>
<script src="/ic_admin/login_form/js/popper.min.js"></script>
<script src="/ic_admin/login_form/js/bootstrap.min.js"></script>
<!-- Extra Plugin CSS -->
<script src="/ic_admin/login_form/vendors/nice-select/js/jquery.nice-select.min.js"></script>

<script src="/ic_admin/login_form/js/theme-dist.js"></script>
<script>
    //    For flash message
    $('div.alert').delay(3000).fadeOut(350);
</script>
</body>
</html>
